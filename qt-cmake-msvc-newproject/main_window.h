﻿#pragma once

#include "ui_main_window.h"

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(QWidget *parent = Q_NULLPTR);
	~MainWindow() = default;

private:
	Ui::MainWindow m_Ui;
};

