# Qt CMake MSVC New Project

Qt CMake MSVC New Project is a simple C++ project to use when you have to start a Qt project from scratch (but not entirely from scratch...).

## Installation

Use the package manager [vcpkg](https://github.com/Microsoft/vcpkg) to install Qt.

```bash
vcpkg.exe install qt5-base:x64-windows-static
```

Next get this project and open it with Microsoft Visual Studio (with CMake support).

Modify the CMakeSettings.json file to change -DCMAKE_TOOLCHAIN_FILE in cmakeCommandArgs to match your vcpkg path

## Usage

Currently this project support only x64 windows static, but you can tweak it to support other platforms.

1. Generate the CMake cache.
2. Build
3. Install

The project contains just an empty MainWindow derived from QMainWindow and the .ui file associated to start your new project.

## License
[MIT](https://choosealicense.com/licenses/mit/)